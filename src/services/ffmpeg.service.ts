import { Injectable } from '@nestjs/common';
import * as ffmpeg from 'fluent-ffmpeg';
import * as ffmpegPath from '@ffmpeg-installer/ffmpeg';
import { path } from 'settings/global.settings';

@Injectable()
export class FfmpegService {
  commander;
  // rtsp://108.58.215.162:554/media/video1
  // ip_port: string = '192.168.6.11:554';
  // inputPath: string = `rtsp://${this.ip_port}/profile2/media.smp`;

  // inputPath: string = 'C:/Users/משתמש/Videos/Desktop/vid1.mp4';
  // inputPath: string = 'C:/Users/משתמש/Desktop/camera-live/hero8.mp4';
  // outputPath: string = 'C:/Users/משתמש/Desktop/camera-live';


  // inputPath: string = `${path}/${videoName}`;
  // outputPath: string = path;

  // hlsSegmentName: string = 'file%03d.ts';
  hlsSegmentName: string = '-%03d.ts';
  hlsTime: number = 3;
  hlsSizeList: number = 0;
  startNumber = 0;
  // hlsSizeList: number = 30;

  constructor() {
    ffmpeg.setFfmpegPath(ffmpegPath.path);
  }
  
  startLive(videoName: string) {
    this.commander = new ffmpeg();
    this.commander
      .input(`${path}/${videoName}/${videoName}.mp4`)
      .outputOptions([
        // options
        '-c: copy',

        // '-c:v libx264',
        // '-preset veryfast',
        // '-crf 20',
        // '-g 15',
        // '-sc_threshold 0',
        // '-ac 2',


        // hls
        `-start_number ${this.startNumber}`, 
        `-hls_time ${this.hlsTime}`,
        `-hls_list_size ${this.hlsSizeList}`,
        '-f hls',
        `-hls_segment_filename ${path}/${videoName}/${videoName}${this.hlsSegmentName}`,
        // `-hls_flags delete_segments+temp_file+append_list`,
        // means that: segments are deleted agter a period of time + writing to a temporary file and rename + removing old segments from list
      ])
      .output(`${path}/${videoName}/${videoName}.m3u8`)
      .on('start', commandLine => {
        console.log('Spawned ffmpeg with command: ' + commandLine);
      })
      .on('error', (err, stdout, stderr) => {
        // console.log('process stopped')
        console.log('An error occured: ' + err.msg, err, stderr);
      })
      .on('progress', progress => {
        // console.log('Processing...')
      })
      .on('end', () => {
        console.log('Finished processing!');
      })
      .run();
  }

  stopLive() {
    this.commander.kill('SIGTERM');
    console.log('stop live');
  }

  resumeLive() {
    this.commander.run();
    console.log('resume live');
  }

  killLive() {
    this.commander.kill();
    console.log('kill live');
  }
}
