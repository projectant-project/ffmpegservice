import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { VideoController } from './video/video.controller';
import { FfmpegService } from './services/ffmpeg.service';


@Module({
  controllers: [AppController, VideoController],
  providers: [AppService, FfmpegService],
})
export class AppModule {}
